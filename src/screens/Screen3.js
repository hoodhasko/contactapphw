import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {useAppState} from '@react-native-community/hooks';
import {useNavigation} from '@react-navigation/native';

import Header from '../components/Header';

const Screen3 = () => {
  const currentAppState = useAppState();
  const navigation = useNavigation();

  useEffect(() => {
    if (currentAppState === 'background') {
      navigation.navigate('Screen 2');
    }
  });
  return (
    <View style={styles.root}>
      <Header title="Gallery" backButton />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  scrollStyles: {
    alignItems: 'center',
    paddingVertical: 20,
  },
  indicatorStyle: {
    flex: 1,
  },
});

export default Screen3;
