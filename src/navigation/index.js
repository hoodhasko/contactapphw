import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

import Screen1 from '../screens/Screen1';
import Screen2 from '../screens/Screen2';
import Screen3 from '../screens/Screen3';

import Tabbar from '../components/Tabbar';

const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabScreens = () => (
  <Tabs.Navigator
    headerMode="none"
    initialRouteName="Screen 1"
    tabBar={({navigation, state, descriptors}) => (
      <Tabbar {...{navigation, state, descriptors}} />
    )}>
    <Tabs.Screen name="Screen 1" component={Screen1} />
  </Tabs.Navigator>
);

const Navigation = () => (
  <NavigationContainer>
    <RootStack.Navigator initialRouteName="Screen 1" headerMode="none">
      <RootStack.Screen name="Screen 1" component={TabScreens} />
      <RootStack.Screen name="Screen 2" component={Screen2} />
      <RootStack.Screen name="Screen 3" component={Screen3} />
    </RootStack.Navigator>
  </NavigationContainer>
);

export default Navigation;
