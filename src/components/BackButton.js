import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import backIcon from '../assets/back-arrow.png';

const BackButton = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.backBtn}
      onPress={() => navigation.goBack(-1)}>
      <Image style={styles.backIcon} source={backIcon} resizeMode={'center'} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  backBtn: {
    height: 36,
    width: 36,
    backgroundColor: '#00ADD3',
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 11,
  },
  backIcon: {
    height: 18,
    width: 12,
  },
});

export default BackButton;
