import React, {useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {useAppState} from '@react-native-community/hooks';

const screenStrings = ['Screen 1', 'Screen 2', 'Screen 3'];

const Tabbar = () => {
  const currentAppState = useAppState();

  const clearStorage = async value => {
    try {
      await AsyncStorage.removeItem('login');
    } catch (e) {
      console.log(e);
    }
  };

  const getUserLogin = async () => {
    try {
      const value = await AsyncStorage.getItem('login');
      if (value !== null) {
        console.log(value);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (currentAppState === 'background') {
      clearStorage();
      getUserLogin();
    }
  });

  console.log('currentAppState', currentAppState);

  //const Tabbar = ({setActiveScreen}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.tabStyle}>
      {screenStrings.map((item, index) => (
        <TouchableOpacity
          key={index}
          style={styles.buttonTabStyle}
          onPress={() => navigation.navigate(item)}>
          {/* onPress={() => setActiveScreen(index + 1)}> */}
          <Text style={styles.tabText}>{item.toUpperCase()}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  tabStyle: {
    marginBottom: 10,
    backgroundColor: '#0A0A0A',
    borderRadius: 20,
    width: '70%',
    height: 50,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 40,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  tabText: {
    fontSize: 15,
    fontWeight: '700',
    color: '#FFFFFF',
    textAlign: 'center',
  },
  buttonTabStyle: {
    flex: 1,
  },
});

export default Tabbar;
