import React from 'react';
import {Text, SafeAreaView, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import BackButton from './BackButton.js';

const Header = ({title, backButton}) => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.headerStyle}>
      {backButton && <BackButton />}
      <Text style={styles.headerText}>{title}</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 25,
  },
  headerText: {
    fontSize: 42,
    fontWeight: '200',
  },
});

export default Header;
